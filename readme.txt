Indexing and fulltext search witch solr:

http://g-rossolini.developpez.com/tutoriels/solr/


solrindex:  bin/nutch solrindex http://localhost:8983/solr/nutch/ crawl/crawldb crawl/linkdb crawl/segments/

solr delete/clean:  bin/nutch clean -Dsolr.server.url=http://127.0.0.1:8983/solr/nutch crawl/crawldb/
SolrIndexer: deleting 3/3 documents

cmd nutch: https://wiki.apache.org/nutch/CommandLineOptions


dump: runtime/local$ bin/nutch dump -segment crawl/segments -outputDir crawl/dump/


1:
bin/nutch inject crawl/crawldb urls
2:
bin/nutch generate crawl/crawldb crawl/segments
3:
s1=`ls -d crawl/segments/2* | tail -1`
4:
bin/nutch fetch $s1
5:
bin/nutch parse $s1
6:
bin/nutch updatedb crawl/crawldb $s1
7:
bin/nutch generate crawl/crawldb crawl/segments -topN 1000
s2=`ls -d crawl/segments/2* | tail -1`
echo $s2

bin/nutch fetch $s2
bin/nutch parse $s2
bin/nutch updatedb crawl/crawldb $s2

8:

bin/nutch generate crawl/crawldb crawl/segments -topN 1000
s3=`ls -d crawl/segments/2* | tail -1`
echo $s3

bin/nutch fetch $s3
bin/nutch parse $s3
bin/nutch updatedb crawl/crawldb $

9:
bin/nutch invertlinks crawl/linkdb -dir crawl/segments
10:
dump: runtime/local$ bin/nutch dump -segment crawl/segments -outputDir crawl/dump/
11:
bin/nutch solrindex http://localhost:8983/solr/nutch/ crawl/crawldb crawl/linkdb crawl/segments/
